#include <random>
#include <chrono>
#include "Coder.hpp"

class EasyCoder::EasyCoder() : Coder{
public:
    /**
     * @brief genera un codice di numeri da 0 a 5 (inclusi) 
     * 
     */
    void GenerateCode(){
            
            using namespace std;
            unsigned seed = chrono::system_clock::now().time_since_epoch().count();
            default_random_engine generator(seed);
            uniform_int_distribution<int> distribution(0, 5);
            for(int i=0;i<4;i++){
                Coder::code[i]=distribution(generator);
            }   

    }

}