class PlayHandler{
    public:
    PlayHandler GetInstance();
    void NewGame(int diffLevel);
    bool CheckCode(int *playerGuess[4]);
    int *GetHints(int *playerGuess[4]);

}