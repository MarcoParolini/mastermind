#include "Coder.hpp"
#include "ScoreCalculator.hpp"

/**
 * @brief 
 * Classe che gestisce la partita. E' costruita secondo il design pattern singleton,
 * in modo che ne esista una sola istanza per l'applicazione. 
 * 
 */
 class PlayHandler{

    public:
    static PlayHandler *instance;
    private: int score=0;
     int difficultyLevel=0;
     int maxTries=0;
     int scoreMultiplier=0;
     Coder *theCode;
    
    public:
    /**
     * @brief Ritorna l'unica istanza di PlayHandler se esistente, la crea altrimenti.
     * 
     * @return * PlayHandler 
     */
     PlayHandler* GetInstance(){
        
        if (instance == nullptr) {
            instance=new PlayHandler();
            return instance;
        }
        else
        {
            return instance;
        }
        
        
    }

	 /**
	 * @brief Crea la partita creando il Coder del livello corretto
	 *
	 * @return * PlayHandler
	 */
    void NewGame(int diffLevel){
        difficultyLevel=diffLevel;
        switch (difficultyLevel){

            case 0:
            //Livello principiante
            maxTries=20;
            scoreMultiplier=1;
            theCoder = new EasyCoder();
            break;

            case 1:
            //Livello normale
            maxTries=14;
            scoreMultiplier=2.5;
            theCoder = new Coder();
            break;

            case 2:
            //Livello esperto
            maxTries=9;
            scoreMultiplier=6;
            theCoder = new HardCoder();
            break;
        }

        theCoder.GenerateCode();
    }

	
    

} ;