template <typename T>
class ScoreCalculator
{
private:
	T score;
	T multiplier;
	int maxTries;
public:
	ScoreCalculator(typename T,T multiplier,int maxTries) 
	{
		score = new T;
		this->multiplier = multiplier;
		this->maxTries = maxTries;
		
	}

	void SetMaxScore() 
	{
		this->score = maxTries * scoreMultiplier;
	}

	void UpdateScore() 
	{
		score -= scoreMultiplier;
	}

	T GetScore() 
	{
		return score;
	}
};
template class ScoreCalculator<int>;
template class ScoreCalculator<float>;