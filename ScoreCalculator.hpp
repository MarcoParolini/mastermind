template <typename T>
class ScoreCalculator
{
public:
	void SetMaxScore();
	void UpdateScore();
	T GetScore();
}