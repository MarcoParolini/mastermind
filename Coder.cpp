#include <random>
#include <chrono>
#include "Coder.hpp";

/**
 * @brief Classe usata per generare il codice, eseguire i controlli e dare i suggerimenti.
 * 
 */
class Coder{
    
    protected:
    int* code=new int[4];

    public:
    /**
     * @brief richiama la giusta funzione di generazione del codice a seconda del livello di difficoltà
     * 
     * @param nLevel indica il livello di difficoltà
     */
     void GenerateCode(){
        using namespace std;
        unsigned seed = chrono::system_clock::now().time_since_epoch().count();
        default_random_engine generator(seed);
        uniform_int_distribution<int> distribution(0, 7);
        for(int i=0;i<4;i++){
            code[i]=distribution(generator);
        }

    }

    /**
     * @brief Controlla se il tentativo dell'utente è corretto
     * 
     * @param playerGuess array contenente il tentativo dell'utente
     * @return true se il tentativo è correttp
     * @return false se il tentativo non è corretto.
     */
    bool CheckCode(int *playerGuess[4]){
        for(int i=0; i<4;i++){
            if (! playerGuess[i] == code[i]){
                return false;
            }
        }
        return true;
    }

    /**
     * @brief Genera un array di suggerimenti.
     * 
     * @param playerGuess array contenente il tentativo dell'utente
     * @return int* array di 2 caselle contenenti rispettivamente:
     * -il numero di numeri giusti nella giusta posizione
     * -il numero di numeri giusti ma nella posizione sbagliata
     */
    int *GetHints(int *playerGuess[4]){
        int *hints= new int[2]();

        for(int i=0; i<4;i++){
            if(*playerGuess[i] == code[i]){
                hints[0]++;
            }else{
                for (int j=0;j<4;j++){
                    if(*playerGuess[i]== code[j]){
                        hints[1]++;
                        break;
                    }
                }
            }
        }

        return hints;

    }

}