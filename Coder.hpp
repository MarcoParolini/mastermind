class Coder{
    public:
    void GenerateCode();
    bool CheckCode(int *playerGuess[4]);
    int *GetHints(int *playerGuess[4]) ;
    protected:
    int* code;
};

class EasyCoder
{
    public:
    void GenerateCode();
};
class HardCoder
{
    public:
    void GenerateCode();
}